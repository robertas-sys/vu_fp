module TicTacToe.Actions.Defend 
where

import Data.Ix
import Data.List
import Data.Ord

import TicTacToe.Actions.Winner
import TicTacToe.Types

togglePlayer :: Player -> Player
togglePlayer 'x' = 'o'
togglePlayer 'o' = 'x'
togglePlayer _ = error "Invalid player!"

getPossibleMoves :: Player -> Moves
getPossibleMoves player = map (\(oX, oY) -> Move { x = oX, y = oY, player = player}) $ range((0, 0), (2, 2))

getAvailableMoves :: Moves -> Player -> Moves
getAvailableMoves moves player = getAvailableMoves' moves $ getPossibleMoves player
    where
        getAvailableMoves' :: Moves -> Moves -> Moves
        getAvailableMoves' [] availableMoves = availableMoves
        getAvailableMoves' (h:t) availableMoves = 
            let
                col = x h
                row = y h
                availableMoves' = filter(\move ->  x move /= col || y move /= row) availableMoves
            in
                getAvailableMoves' t availableMoves'

defend :: Moves -> Player -> Maybe Move
defend game currentPlayer = 
    let
       (score, move) = minmax game currentPlayer currentPlayer
    in 
        move 

minmax :: Moves -> Player -> Player -> (Int, Maybe Move)
minmax game turnPlayer currentPlayer = minmax' game turnPlayer currentPlayer (winner game) (length game)

minmax' :: Moves -> Player -> Player -> Maybe Player -> Int -> (Int, Maybe Move)
minmax' [] _ currentPlayer _ _  = (0, Just Move {x = 1, y = 1, player = currentPlayer})

minmax' _ _ currentPlayer (Just winner) _ =
    if winner == currentPlayer 
    then (10, Nothing)
    else (-10, Nothing)
    
minmax' _ _ _ Nothing 9 = (0, Nothing)
minmax' game turnPlayer currentPlayer Nothing _ = minmax'' game turnPlayer currentPlayer

minmax'' :: Moves -> Player -> Player -> (Int, Maybe Move)
minmax'' game turnPlayer currentPlayer = 
    let
        availableMoves = getAvailableMoves game turnPlayer
        (scores, moves) = updateScores availableMoves game turnPlayer currentPlayer
        (score, index) = if turnPlayer == currentPlayer
                         then maximumBy (comparing fst) (zip scores [0..])
                         else minimumBy (comparing fst) (zip scores [0..])
        move = moves !! index
    in
        (score, Just move)

updateScores :: Moves -> Moves -> Player -> Player -> ([Int], Moves)
updateScores availableMoves game turnPlayer currentPlayer = 
    updateScores' availableMoves game turnPlayer currentPlayer [] []
    where
        updateScores' :: Moves -> Moves ->  Player -> Player -> [Int] -> Moves -> ([Int], Moves)  
        updateScores' [] _ _ _ scores moves = (scores, moves)
        updateScores' (h:t) game turnPlayer currentPlayer scores moves =
            let             
                possibleGame = game ++ [h]
                toggledPlayer = togglePlayer turnPlayer
                (score, move) = minmax possibleGame toggledPlayer currentPlayer
            in
                updateScores' t game turnPlayer currentPlayer (scores ++ [score]) (moves ++ [h])