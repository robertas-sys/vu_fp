module TicTacToe.Types 
where

type Player = Char

data Move = Move { 
    x :: Int
  , y :: Int
  , player :: Player 
} deriving (Show, Eq)

type Moves = [Move]