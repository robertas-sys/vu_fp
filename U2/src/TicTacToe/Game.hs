module TicTacToe.Game where

import TicTacToe.Types
import TicTacToe.Helpers.Http
import TicTacToe.Actions.Defend
import TicTacToe.Actions.Winner
import TicTacToe.Parsers.Bencode
import TicTacToe.Serializers.Bencode
import Data.Maybe

startGame :: String -> String -> Player -> IO (Maybe Player)
startGame gameId "1" player = do
    let counterMove = defend [] player 
    statusCode <- makeMove gameId "1" [fromJust counterMove]
    runItteration gameId "1" player
startGame gameId playerId player = runItteration gameId playerId player

runItteration :: String -> String -> Player -> IO (Maybe Player)
runItteration gameId playerId player = do
    opponentMoveBencoded <- waitForOpponent gameId playerId
    let moves = parseBencode opponentMoveBencoded
    let counterMove = defend moves player
    statusCode <- makeMove gameId playerId $ moves ++ [fromJust counterMove]
    if isGameOver counterMove 
        then return (winner moves)
        else runItteration gameId playerId player 
      
isGameOver :: Maybe Move -> Bool
isGameOver (Just move) = False
isGameOver Nothing = True

waitForOpponent :: String -> String -> IO String
waitForOpponent gameId playerId = do
    response <- httpGet (buildUrl gameId playerId)
    if response == "Internal Server Error"
    then waitForOpponent gameId playerId
    else return response

makeMove :: String -> String -> Moves -> IO Int
makeMove gameId playerId moves = httpPost (buildUrl gameId playerId) $ serializeBencode moves