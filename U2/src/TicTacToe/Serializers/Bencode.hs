module TicTacToe.Serializers.Bencode
where

import Data.Char
import TicTacToe.Types

serializeBencode :: Moves -> String
serializeBencode moves = serialize' moves 0 ""
    where 
        serialize' :: Moves -> Int -> String -> String
        serialize' [] _ result = "d" ++ result ++ "e"
        serialize' (h:t) iter result =
              serialize' t (iter + 1) $
               result ++
               "1:" ++
               [intToDigit iter] ++
               serializeMove h
              
serializeMove :: Move -> String
serializeMove move = 
    "d" ++ 
    serializePlayer (player move) ++
    serializeCoord 'x' (x move) ++
    serializeCoord 'y' (y move) ++
    "e"

serializePlayer :: Player -> String
serializePlayer player =  "1:v1:"++ [player]

serializeCoord :: Char -> Int -> String
serializeCoord axis value = 
    "1:" ++
    [axis] ++
    "i" ++
    [intToDigit value] ++
    "e"