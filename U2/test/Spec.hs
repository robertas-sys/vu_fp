module Main where
 
import Test.Hspec
import TicTacToe.Actions.Winner
import TicTacToe.Actions.Defend
import TicTacToe.Parsers.Bencode
import TicTacToe.Serializers.Bencode
import TicTacToe.Types
 
main :: IO ()
main = hspec $ do
  describe "TicTacToe.Actions.Winner" $ do
    it "returns Nothing if no winner" $ do
      winner [Move 1 2 'x'] `shouldBe` Nothing

    it "returns winner Just 'x' " $ do
      winner [Move 0 0 'x', 
              Move 0 1 'x',
              Move 0 2 'x'] `shouldBe` Just 'x'  

    it "returns winner Just 'o' " $ do
      winner [Move 0 0 'o', 
              Move 0 1 'o',
              Move 0 2 'o'] `shouldBe` Just 'o'  

  describe "TicTacToe.Actions.Defend" $ do
    it "provides no move if 9 moves were taken" $ do
      defend [Move 0 0 'x', 
              Move 1 0 'o',
              Move 2 0 'x',
              Move 0 1 'x', 
              Move 1 1 'x',
              Move 2 1 'o',
              Move 0 2 'o', 
              Move 1 2 'x',
              Move 2 2 'o'] 'x' `shouldBe` Nothing

    it "blocks opponent attack" $ do
       defend [Move 1 2 'x', 
               Move 1 1 'x', 
               Move 2 2 'o'] 'o' `shouldBe` Just (Move 1 0 'o')

    it "takes first move on the board center" $ do
      defend [] 'x' `shouldBe` Just (Move 1 1 'x')

  describe "Parsers.Bencode" $ do
    it "returns [] if bencode is empty dictionary" $ do
      parseBencode "de" `shouldBe` []

    it "returns [Move {x = 2, y = 2, player 'x'}]" $ do
      parseBencode "d1:0d1:v1:x1:xi2e1:yi2eee" `shouldBe` [Move 2 2 'x']

  describe "Serialize.Bencode" $ do
    it "returns 'de' if no moves in array" $ do
      serializeBencode [] `shouldBe` "de"

    it "returns 'd1:0d1:v1:x1:xi2e1:yi2eee' after serialization" $ do
      serializeBencode [Move 2 2 'x'] `shouldBe` "d1:0d1:v1:x1:xi2e1:yi2eee"

  describe "Parse / Serialize" $ do
    it "returns True if parsed and serialized string equals to original" $ do
      (serializeBencode (parseBencode "d1:0d1:v1:x1:xi2e1:yi2eee")) == "d1:0d1:v1:x1:xi2e1:yi2eee" `shouldBe` True

