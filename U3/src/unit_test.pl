:- begin_tests(winner).
:- use_module(winner).

test('undefined if no winner') :-
        winner(
	    3,
	    3,
	    [(1,2,x)],
	    undefined
	).

test('winner is x') :-
	winner(
	    3,
	    3,
	    [(0,0,x),
	     (0,1,x),
	     (0,2,x)
	     ],
	     x).

test('winner is o') :-
	winner(
	    3,
	    3,
	    [(0,0,o),
	     (0,1,o),
	     (0,2,o)
	     ],
	     o).

:- end_tests(winner).

:- begin_tests(defend).
:- use_module(defend).

test('undefined move if 9 were taken and no winner') :-
	 defend(3,3,
	    [(0,0,x),
             (1,0,o),
             (2,0,x),
             (0,1,x),
	     (1,1,x),
             (2,1,o),
             (0,2,o),
             (1,2,x),
             (2,2,o)], x,undefined).

test('blocks opponent attack') :-
	defend(3,
	       3,
	       [(1,2,x),
	        (1,1,x),
	        (2,2,o)
	       ],
	       o,
	       (1,0,o)).

test('takes first move on the board center') :-
	defend(3,3,[],o,(1,1,o)).

:- end_tests(defend).

:- begin_tests(bencodeParser).
:- use_module(parseBencode).
test('[] if bencode is empty dictionary') :-
	parseBencode(de,[]).

test(' parses one move - (2,2,x)') :-
	parseBencode('d1:0d1:v1:x1:xi2e1:yi2eee',[(2,2,x)]).
:-end_tests(bencodeParser).

:- begin_tests(bencodeSerializer).
:- use_module(serializeBencode).
test('de if no moves in list') :-
	serializeBencode([],de).

test('d1:0d1:v1:x1:xi2e1:yi2eee after serialization') :-
	serializeBencode([(2,2,x)],'d1:0d1:v1:x1:xi2e1:yi2eee').
:- end_tests(bencodeSerializer).

:- begin_tests(parseSerialize).
:- use_module(serializeBencode).
:- use_module(parseBencode).

test('true if parsed and serialized bencode equals to original') :-
	parseBencode('d1:0d1:v1:x1:xi2e1:yi2eee',Parsed),
	serializeBencode(Parsed,Serialized),
	Serialized == 'd1:0d1:v1:x1:xi2e1:yi2eee'.

:- end_tests(parseSerialize).
