:- module(winner, [winner/4, createScores/3]).
:- use_module(library(clpfd)).

%N - Columns, X - axis
%M - Rows, Y - axis
%x - positve score
%o - negative score


winner(N,M,Moves,Winner) :-
	createScores(N,M,Scores),
	itterateMoves(Moves,N,M,Scores,GameScore),
	(  won(GameScore,N,Winner);
	   won(GameScore,M,Winner)
	).

won(GameScore,WinScore,Winner) :-
	(
            member(WinScore,GameScore),
	    Winner = x
	);
	(
	    NegWinScore is WinScore * -1,
	    member(NegWinScore,GameScore),
	    Winner = o
	);
	Winner = undefined.

% [row1,row2,row3,rowM,...,col1,col2,col3,colN,...,diag1,diag2]
createScores(N,M,Out) :-
	ScoresLength is N + M + 2,
	findall(0,between(1,ScoresLength,_),Out).

itterateMoves([],_,_,Scores,Scores).
itterateMoves([Move|Moves],N,M,Scores,Result) :-
	updateScore(Move,N,M,Scores,UpdatedScores),
	itterateMoves(Moves,N,M,UpdatedScores,Result).

updateScore((X,Y,Player),N,M,Scores,UpdatedScores) :-
	addPlayerScore(Y,Player,Scores,ScoresRow),

	ColumnScoreIndex is N + X,
	addPlayerScore(ColumnScoreIndex,Player,ScoresRow,ScoresCol),

	updateDiag1Score(N,M,X,Y,Player,ScoresCol,ScoresDiag1),
	updateDiag2Score(N,M,X,Y,Player,ScoresDiag1,UpdatedScores).

updateDiag1Score(N,M,X,Y,Player,Scores,UpdatedScores) :-
	X =:= Y,
	Diag1Index is N + M,
	addPlayerScore(Diag1Index,Player,Scores,UpdatedScores).
updateDiag1Score(_,_,_,_,_,Scores,Scores).

updateDiag2Score(N,M,X,Y,Player,Scores,UpdatedScores) :-
	N - X - 1 =:= Y,
	Diag2Index is N + M + 1,
	addPlayerScore(Diag2Index,Player,Scores,UpdatedScores).
updateDiag2Score(_,_,_,_,_,Scores,Scores).

addPlayerScore(N,x,Scores,UpdatedScores) :-
	addScore(N,1,Scores,UpdatedScores).

addPlayerScore(N,o,Scores,UpdatedScores) :-
	addScore(N,-1,Scores,UpdatedScores).

addScore(N,Num,Scores,UpdatedScores) :-
	nth0(N,Scores,Score),
	UpdatedScore is Score + Num,
        update(Scores,N,UpdatedScore,UpdatedScores).

update(List,N,With,ListOut) :-
   same_length(List,ListOut),
   append(Prefix,[_Discard|Suffix],List),
   length(Prefix,N),
   append(Prefix,[With|Suffix],ListOut).

update(Es, N, X, Xs) :-
   updateElAt(Es, 0,N, X, Xs).

updateElAt([_|Es], I ,I, X, [X|Es]).
updateElAt([E|Es], I0,I, X, [E|Xs]) :-
   I0 #< I,
   I1 #= I0+1,
   updateElAt(Es, I1,I, X, Xs).
