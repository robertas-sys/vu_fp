:- module(http, [httpGet/2,httpPost/2,buildUrl/3]).
:- use_module(library(http/mimepack)).
:- use_module(library(http/http_open)).

:- set_prolog_flag(report_error,true).
:- set_prolog_flag('httpGet/2',error).

httpPost(Url,Message) :-
	    http_open(
		      Url
		     ,In
		     ,[post(atom(Message))
		     ,request_header('Content-Type'='application/bencode+map')]
	    ),
	    close(In).

httpGet(Url,Result) :-
	    catch(
	        setup_call_cleanup(
		    http_open(Url,In,[request_header('Accept'='application/bencode+map')]),
		    read_string(In, "\n", "\r", _, Result),
		    close(In)
	        ),
		error(Err,_),
		(
		    Result = undefined,
		    format('Server responded with an error: ',[Err])
		)
	    ).

buildUrl(GameId,Player,Url) :-
	atomic_list_concat([
	    'http://tictactoe.homedir.eu/game/',
	    GameId,
	    '/player/',
	    Player
	  ]
	,Url).
