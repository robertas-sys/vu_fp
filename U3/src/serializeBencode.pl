:- module(serializeBencode,[serializeBencode/2]).

serializeBencode(Moves,Result) :-
	serialize(Moves,0,[],Atoms),
	atomic_list_concat(Atoms,Result).

serialize([],_,Result, Out) :-
	append([d],Result,Out1),
	append(Out1,[e],Out).
serialize([H|T],Iter,Result,R) :-
	IterNext is Iter + 1,
	serializeMove(H,Move),
	append(Result,[1,:,Iter],Out1),
	append(Out1,Move,Out2),
	serialize(T,IterNext,Out2,R).

serializeMove((X,Y,P),Result) :-
	serializePlayer(P,Player),
	serializeCoord(x,X,CoordX),
	serializeCoord(y,Y,CoordY),
	append([d],Player,Out1),
	append(Out1,CoordX,Out2),
	append(Out2,CoordY,Out3),
	append(Out3,[e],Result).

serializePlayer(Player,[1,:,v,1,:,Player]).
serializeCoord(Axis,Value,[1,:,Axis,i,Value,e]).
