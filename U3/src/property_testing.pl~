:- use_module(library(quickcheck)).
:- use_module(library(lists)).
:- use_module(library(tap)).

:- begin_tests(serialize).
:- use_module(serializeBencode).
:- use_module(parseBencode).

% define rules for type of natural number
:- multifile error:has_type/2.
error:has_type(move, (_,_,_)).

% Move	generator
:- multifile quickcheck:arbitrary/2.
quickcheck:arbitrary(move, (X,Y,P)) :-
    arbitrary(between(0,9),X),
    arbitrary(between(0,9),Y),
    arbitrary(between(0x61,0x7a),Code),
    atom_codes(P,[Code]).

prop_serialize(R:list(move)) :-
	serializeBencode(R, Serialized),
	parseBencode(Serialized, R).

test('serialized and parsed back bencode equals to its original') :-
	quickcheck(prop_serialize/1).

:- end_tests(serialize).


:- begin_tests(winner).
:- use_module(winner).

% define rules for type of natural number

prop_createScores(N:positive_integer,M:positive_integer) :-
	createScores(N,M,Scores),
        length(Scores,Len),
	LenShouldBe is N + M + 2,
	LenShouldBe =:= Len.

test('createScores should always return board with N + M + 2 0 elements') :-
	quickcheck(prop_createScores/2).

:- end_tests(winner).


:- begin_tests(demo).

% reversing a list leaves its length the same
prop_reverse_length(L:list(integer)) :-
    length(L, Len),
    reverse(L, RL),
    length(RL, Len).

% a property that's never true
prop_nonsense(A:list(integer), B:list(integer)) :-
    length(A, LenA),
    length(B, LenB),
    LenA =< LenB.

% atom_length/2 and length/2 agree
prop_atom_lengths(A:atom) :-
    atom_length(A, Len),
    atom_codes(A, Codes),
    length(Codes, Len).


test('reverse does not change length') :-
    quickcheck(prop_reverse_length/1).

test('always fails') :-
    quickcheck(prop_nonsense/2).

test('atom_length/2 and length/2 agree') :-
    quickcheck(prop_atom_lengths/1).

:- end_tests(demo).








