:- module(game,[startGame/4]).

:- use_module(winner).
:- use_module(defend).
:- use_module(parseBencode).
:- use_module(http).
:- use_module(serializeBencode).

startGame(GameId,1,Tile,Winner) :-
	defend(3,3,[],Tile,CounterMove),
	append([],[CounterMove],Game),
	makeMove(GameId,1,Game),
	runIteration(GameId,1,Tile,Winner).

startGame(GameId,PlayerId,Tile,Winner) :-
	runIteration(GameId,PlayerId,Tile,Winner).

runIteration(GameId,PlayerId,Tile,Winner) :-
	waitForOpponent(GameId,PlayerId,OpponentMovesBencoded),
	parseBencode(OpponentMovesBencoded,Moves),
	defend(3,3,Moves,Tile,CounterMove),
	append(Moves,[CounterMove],Game),
	makeMove(GameId,PlayerId,Game),
	(
	    (
	        CounterMove == undefined,
	        winner(3,3,Game,Winner)

	    ) ;
	    runIteration(GameId,PlayerId,Tile,Winner)
	).

waitForOpponent(GameId,PlayerId,Result) :-
	buildUrl(GameId,PlayerId,Url),
	httpGet(Url,R),
	(
	    (
	        R == undefined,
	        waitForOpponent(GameId,PlayerId,Result)
	    ) ;

	    Result = R
	).

makeMove(GameId,PlayerId,Moves) :-
	buildUrl(GameId,PlayerId,Url),
	serializeBencode(Moves,Message),
	httpPost(Url,Message).
