:- module(defend, [defend/5]).
:- use_module(winner).
:- use_module(library(clpfd)).

%N - Columns, X - axis
%M - Rows, Y - axis

defend(N,M,Game,CurrentPlayer,Move) :-
	minmax(N,M,Game,CurrentPlayer,CurrentPlayer,(_,Move)).

minmax(N,M,Game,TurnPlayer,CurrentPlayer,(Score,Move)) :-
	winner(N,M,Game,Winner),
	length(Game,MovesTaken),
	minmax1(N,M,Game,TurnPlayer,CurrentPlayer,Winner,(Score,Move),MovesTaken).

minmax1(_,_,_,_,_,undefined,(0,undefined),9).
minmax1(_,_,[],_,CurrentPlayer,_,(Score,Move),_) :-
	Score = 0,
	Move = (1,1,CurrentPlayer).

minmax1(N,M,Game,TurnPlayer,CurrentPlayer,undefined,(Score,Move),_) :-
	minmax2(N,M,Game,TurnPlayer,CurrentPlayer,(Score,Move)).

minmax1(_,_,_,_,CurrentPlayer,Winner,(Score,undefined),_) :-
	(
          Winner == CurrentPlayer,
	  Score = 10
	) ;

	Score = -10.

minmax2(N,M,Game,TurnPlayer,CurrentPlayer,(Score,Move)) :-
	getAvailableMoves(N,M,Game,TurnPlayer,AvailableMoves),
	updateScores(N,M,AvailableMoves,Game,TurnPlayer,CurrentPlayer,(Scores,Moves)),
	(
	    (
	        TurnPlayer == CurrentPlayer,
	        maximum_at(Scores,Score,Index),
	        nth0(Index,Moves,Move)
	    ) ;
	    (
	        minimum_at(Scores,Score,Index),
	        nth0(Index,Moves,Move)
	    )
	).

updateScores(N,M,AvailableMoves,Game,TurnPlayer,CurrentPlayer,(Scores,Moves)) :-
	makeMove(N,M,AvailableMoves,Game,TurnPlayer,CurrentPlayer,[],[],(Scores,Moves)).

makeMove(_,_,[],_,_,_,Scores,Moves,(Scores,Moves)).
makeMove(N,M,[AvailableMove|AvailableMoves],Game,TurnPlayer,CurrentPlayer,Scores,Moves,Result) :-
	append(Game,[AvailableMove],PossibleGame),
	togglePlayer(TurnPlayer,ToggledPlayer),
	minmax(N,M,PossibleGame,ToggledPlayer,CurrentPlayer,(Score,_)),
	append(Scores,[Score],ScoresUpdated),
	append(Moves,[AvailableMove],MovesUpdated),
	makeMove(N,M,AvailableMoves,Game,TurnPlayer,CurrentPlayer,ScoresUpdated,MovesUpdated,Result).

maximum_at(List,Max,Pos) :-
   maplist(#>=(Max),List),
   nth0(Pos,List,Max).

minimum_at(List,Min,Pos) :-
   maplist(#=<(Min),List),
   nth0(Pos,List,Min).

togglePlayer(o, x).
togglePlayer(x, o).

makeSubList(Num,1,Player,List) :- append([],[(Num,0,Player)],List).
makeSubList(Num,M,Player,List) :- M > 0,
	                          M1 is M-1,
	                          makeSubList(Num,M1,Player,List1),
		                  append(List1,[(Num,M1,Player)],List).

getPossibleMoves(0,_,_,[]).
getPossibleMoves(N,M,Player,List) :- N >= 0,
	                     N1 is N-1,
		             makeSubList(N1,M,Player,L),
		             getPossibleMoves(N1,M,Player,List1),
			     append(List1,L,List).

isCellTaken((X,Y,_),(A,B,_)) :-
	X == A,
	Y == B.

filterPossibleMoves([], Moves, Moves).
filterPossibleMoves([H|T],PossibleMoves,Res) :-
	exclude(isCellTaken(H),PossibleMoves,Out),
	filterPossibleMoves(T,Out,Res).

getAvailableMoves(N,M,MovesTaken,Player,Result) :-
	getPossibleMoves(N,M,Player,PossibleMoves),
	filterPossibleMoves(MovesTaken,PossibleMoves,Result).
