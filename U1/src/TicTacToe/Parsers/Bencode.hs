module TicTacToe.Parsers.Bencode where

import Data.Char
import Data.Maybe
import TicTacToe.Messages.Bencode

data Move = Move { 
    x :: Int
  , y :: Int
  , player :: Char 
} deriving Show

type Moves = [Move]

readDigit :: String -> (Char, String)
readDigit ('0':rest) = ('0', rest)
readDigit ('1':rest) = ('1', rest)
readDigit ('2':rest) = ('2', rest)
readDigit _ = error "Valid digit expected"

readPlayer :: String -> (Char, String)
readPlayer ('x':rest) = ('x', rest)
readPlayer ('o':rest) = ('o', rest)
readPlayer _ = error "Valid player expected"
 
skipSeparator :: String -> String
skipSeparator (d:':':rest) = 
    case isDigit(d) of
      True -> rest
      False -> "Valid separator expected"

readKey :: String -> (Char, String)
readKey ('v':rest) = ('v', rest)
readKey ('x':rest) = ('x', rest)
readKey ('y':rest) = ('y', rest)
readKey (h:t) = case isDigit(h) of
                     True -> (h, t)
                     False -> error "Valid key expected"

readValueByKey :: Char -> String -> (Char, String)
readValueByKey 'v' rest = readPlayer rest 
readValueByKey 'x' rest = parseDigit rest
readValueByKey 'y' rest = parseDigit rest
readValueByKey _ _ = error "Defined key expected"

--d1:0d1:v1:x1:xi1e1:yi0eee
parseBencode :: String -> Moves
parseBencode ('d':rest) = reverse $ parseDicts [] rest
parseBencode _ = error "Dictionary expected"

--1:0d1:v1:x1:xi1e1:yi0eee
parseDicts :: Moves -> String -> Moves
parseDicts result "e" = result
parseDicts result rest =
    let 
        skipRest = skipSeparator rest
        (key, keyRest) = readKey skipRest 
        (move, parseMoveRest) = parseMove keyRest
    in 
        parseDicts (move:result) parseMoveRest

--d1:v1:x1:xi1e1:yi0ee
parseMove :: String -> (Move, String)
parseMove ('d':rest) =
    let
        (keyVal1, rest1) = parseKeyValue rest
        (keyVal2, rest2) = parseKeyValue rest1
        (keyVal3, rest3) = parseKeyValue rest2

        keyVals = [keyVal1, keyVal2, keyVal3]       
    in
        case (rest3) of
             ('e':t) -> (createMove keyVals, t)
             _       -> error "Dictionary without enclosing 'e'"
parseDict _ = error "Dictionary expected"

createMove :: [(Char, Char)] -> Move
createMove keyVals = 
    let
        x = digitToInt $ fromJust $ lookup 'x' keyVals
        y = digitToInt $ fromJust $ lookup 'y' keyVals 
        player = fromJust $ lookup 'v' keyVals
    in
        Move { x = x , y = y, player = player }

-- 1:v1:x // 1:xi1e // 1:yi0e
parseKeyValue :: String -> ((Char, Char), String)
parseKeyValue msg =
    let 
        skipSepRest1 = skipSeparator msg
        (key, readKeyRest) = readKey skipSepRest1

        rest = if isDigit(head readKeyRest) 
               then skipSeparator readKeyRest
               else readKeyRest

        (value, readKeyValRest) = readValueByKey key rest 
    in
        ((key, value), readKeyValRest)

-- i1e
parseDigit :: String -> (Char, String)
parseDigit ('i':rest) =
    let
       (digit, readDigitRest) = readDigit rest
    in
        case readDigitRest of
            ('e':t) -> (digit, t)
            _       -> error "Digit without enclosing 'e'"
parseDigit _ = error "Digit expected"