module TicTacToe.Messages.Bencode
where

{-
message to find out a winner
board:
+-+-+-+
|X| |X|
+-+-+-+
|X|O| |
+-+-+-+
|X|O|O|
+-+-+-+
-}
message :: String
message = "d1:0d1:v1:x1:xi1e1:yi0ee1:1d1:v1:o1:xi2e1:yi1ee1:2d1:v1:x1:xi0e1:yi2ee1:3d1:v1:o1:xi1e1:yi1ee1:4d1:v1:x1:xi2e1:yi0ee1:5d1:v1:o1:xi2e1:yi2ee1:6d1:v1:x1:xi0e1:yi0eee"