module TicTacToe.Actions.Winner 
where

import TicTacToe.Parsers.Bencode
import Data.Sequence as S

-- [row1, row2, row3, row(n), ..., col1, col2, col3, col(n), ..., diag1, diag2]
type Scores = Seq(Int)

boardSize = 3

-- scores length = 2 * boardSize (1xrows + 1xcols) + 2 (diagonals) 
createScores :: Int -> Scores
createScores boardSize = S.replicate (boardSize * 2 + 2) 0

winner :: String -> Maybe Char
winner message = winner' (parseBencode message) $ createScores boardSize
    where 
        winner' :: Moves -> Scores -> Maybe Char
        winner' [] scores = Nothing
        winner' (h:t) scores = 
            let
                (winner, updatedScores) = updateScore h scores
            in
                case winner of
                    Just player -> winner
                    Nothing -> winner' t updatedScores 

updateScore :: Move -> Scores -> (Maybe Char, Scores)
updateScore move scores =
    let 
        col = x move
        row = y move
        playerr = player move

        (scoreRow, scoresRow) = addPlayerScore row playerr scores
        isRowWin = isWinnerMove scoreRow

        (scoreCol, scoresCol) = addPlayerScore (col + boardSize) playerr scoresRow
        isColWin = isWinnerMove scoreCol

        (scoreDiag1, scoresDiag1) = if row == col
                                    then addPlayerScore (2 * boardSize) playerr scoresCol
                                    else (scoreCol, scoresCol)
        isDiag1Win = isWinnerMove scoreDiag1

        (scoreDiag2, scoresDiag2) = if (boardSize - col - 1 == row)
                                    then addPlayerScore (2 * boardSize + 1) playerr scoresDiag1
                                    else (scoreDiag1, scoresDiag1)
        isDiag2Win = isWinnerMove scoreDiag2
    in 
        case isRowWin || isColWin || isDiag1Win || isDiag2Win of
            True -> (Just playerr, scoresDiag2)
            False -> (Nothing, scoresDiag2)

isWinnerMove :: Int -> Bool 
isWinnerMove score = score /= 0 &&
                     mod score boardSize == 0

addPlayerScore :: Int -> Char -> Scores -> (Int, Scores)
addPlayerScore n 'x' scores = addScore n 1 scores
addPlayerScore n 'o' scores = addScore n (-1) scores
addPlayerScore _ _ _ = error "Valid player expected" 

addScore :: Int -> Int -> Scores -> (Int, Scores)
addScore n num scores = 
    let
        updatedScore = (index scores n + num)
        updatedScores = update n updatedScore scores
    in
        (updatedScore, updatedScores)