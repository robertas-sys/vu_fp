Setup source folder in .ghci file;

http://stackoverflow.com/questions/10310166/haskell-can-only-load-one-file-at-a-time-via-load
To load and use several packages simultaneously use the following commands:

ghci 
:load TicTacToe
winner message

OR

gchi
:load TicTacToe.Actions.Winner 
winner message

OR (if module TicTacToe.Actions.Winner does not import any dependacies)

gchi
:load TicTacToe.Parsers.Bencode  TicTacToe.Messages.Bencode TicTacToe.Actions.Winner
:module TicTacToe.Parsers.Bencode TicTacToe.Messages.Bencode TicTacToe.Actions.Winner
winner message